param (
    [string]$CertFilePath,
    [string]$CertPassword
 )
$cert = Import-PfxCertificate -FilePath "$CertFilePath" -Password (ConvertTo-SecureString -String "$CertPassword" -AsPlainText -Force) -CertStoreLocation Cert:\LocalMachine\My
Export-Certificate -Cert $cert -File c:\cert.sst -Type SST
Import-Certificate -File c:\cert.sst  -CertStoreLocation Cert:\LocalMachine\Root
Import-Certificate -File c:\cert.sst -CertStoreLocation Cert:\CurrentUser\My